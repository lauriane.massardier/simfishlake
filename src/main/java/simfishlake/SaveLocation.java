/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake;
import java.io.File;
import java.io.FileWriter;
import java.text.DecimalFormat;

import simfishlake.fishes.Fish;
import simfishlake.fishes.FishesGroup;
import simfishlake.fishes.FishTrackLocation;
import simfishlake.fishes.FishesPopulateProcess;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import java.io.IOException;

/**
 *		Class that saves the position of individuals at the end of the simulation
 *		The positions and the date corresponding to each one are recorded in an output file
 *		
 *		Extract of output file  :
 *		TimeStep 		 Date 		Ind 1 	Ind 2 	Ind 3 	Ind 4 	Ind 5 	Ind 6 	Ind 7 	...
 *		1		2012/01/01/01	4530	5391	5606	5392	52066	52282	51444	...
 *		
 *		@author Guillaume GARBAY and Lauriane MASSARDIER
 *		@version 1.0
 */
public class SaveLocation extends AquaNismsGroupProcess<Fish, FishesGroup> {

	/**
	 *		Process which, at the end of the simulation, saves the position of the individuals in an output file
	 *		@param group all individuals
	 *		@return /
	 */
	@Override
	public void doProcess (FishesGroup group) {
		
		String line = null;
		DecimalFormat df = new DecimalFormat("00");
		
		// Declaration of output file
		final String dirpath = "data/output/Positions.txt";
		final File file = new File(dirpath);
		
		try {
			// creation of file
			file.createNewFile();
			
			// Creation of writer
			final FileWriter writer = new FileWriter(file);
			try {
				// Creating the file header line
				line = "PasTps\tDate\t";
				for (int i=1; i!=Fish.cptIndividu; i++) {
					line = line + "Ind " + i + "\t";
				}
				writer.write(line + "\r");
				
				// Write each line of data (date + positions)
				for (int compteur = 0; compteur < FishTrackLocation.trackLocation[1].length; compteur++) {
					
					// Write timestep 		 	
					line = String.valueOf(FishTrackLocation.trackDate[0][compteur]) + "\t";
					line = line + FishTrackLocation.trackDate[1][compteur] + "/" +
									df.format(FishTrackLocation.trackDate[2][compteur]) + "/" + 
									df.format(FishTrackLocation.trackDate[3][compteur]) + "/" + 
									df.format(FishTrackLocation.trackDate[4][compteur]) + "\t";
					
					// Write position
					for (int i=1; i<FishTrackLocation.trackLocation.length; i++) {
						line = line + FishTrackLocation.trackLocation[i][compteur] + "\t";;
					}
					line = line + "\n";
                                        //System.out.println("ligne "+ligne);
					writer.write(line);
				}	
			} finally {
				writer.close(); // close file
			}
		} catch (IOException e) { // Error creating file
			System.out.println("Sorry, can not create the position record file.");
		}
	}	
}

