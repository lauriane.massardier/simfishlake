/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake;

import java.io.File;
import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.io.FileNotFoundException;

import simfishlake.Cell;
import simfishlake.fishes.Fish;
import simfishlake.environment.Time;
import simfishlake.environment.AreaMovement;
import simfishlake.environment.WaterLevelFluctuationFile;

import fr.cemagref.simaqualife.pilot.Pilot;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.extensions.spatial2D.Grid2D;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;

/**
 * Class that initializes the simulation grid (lack and cells
 * not accessible)
 *
 * @author Guillaume GARBAY and Lauriane Massardier-Galat�
 * @version 1.0
 */
public class Grid extends Grid2D<Cell, Individual> {

    private String waterLevelInit = "513_4";
    private int cellnumber_2 = (gridWidth * gridHeight) - 2;
    

    /**
     * Builder of Grid class 
     *
     * @param gridWidth Total width of the grid
     * @param gridHeight Total height of the grid
     * @param neighborsType neighbors type used
     * @return /
     */
    public Grid(int gridWidth, int gridHeight, NeighborsType neighborsType) {
        super(gridWidth, gridHeight, neighborsType);
        
        // Auto-generated constructor stub
    }

    /**
     * Declaration and Initialization of the grid with HSI of each cell
     * called during the initialization of the program
     *
     * @param pilot
     * @return /
     */
    @InitTransientParameters
    public void initTransientParameters(Pilot pilot) throws FileNotFoundException {

        StringTokenizer sLigne;
        @SuppressWarnings("unused")
        double hsiStd = 0, hsiMean = 0;
        int idCell = 0, yFish = 0, xFish = 0;

        // Declaration of empty grid
        grid = new Cell[gridWidth * gridHeight];
        //System.out.print(gridWidth + " " + gridHeight);

        // Initialization of all the grid with hsi = -1
        for (int cptCell = 0; cptCell < (gridWidth * gridHeight - 1); cptCell++) {
            grid[cptCell] = new Cell(cptCell, -1);
        }

        waterLevelInit = WaterLevelFluctuationFile.dateWaterLevel[Time.dayOfMonth][Time.month][Time.hour];
        if (waterLevelInit == null) {
            waterLevelInit = "513_4";
        }

        // Read file containing the HSI of all cell
        String filePath = "data/input/HSI/hsi_BRO" + Time.getSeason() + waterLevelInit + ".txt";
        Scanner scanner = new Scanner(new File(filePath));
        
    	// Initialization of each cell containing HSI
        // loop
        String line = scanner.nextLine();
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();

            // Cut line : id, hsiStd, hsiMean
            sLigne = new StringTokenizer(line);
            if (sLigne.hasMoreTokens()) {
                idCell = Integer.parseInt(sLigne.nextToken()) - 1;
            }
            if (sLigne.hasMoreTokens()) {
                hsiStd = Double.parseDouble(sLigne.nextToken());
            }
            if (sLigne.hasMoreTokens()) {
                hsiMean = Double.parseDouble(sLigne.nextToken());
            }

    		// Conversion idCell in coordinates (x, y) 
            // (x, y) with id of cell : 0 to n-1
            yFish = (int) Math.floor(idCell / gridWidth);
            xFish = (int) idCell - (yFish * gridWidth);

            // Inversion of coordinate in Y (place the lower left origin)
            yFish = (gridHeight - 1) - yFish;
            // Conversion in coordinate (x, y) in idCell 
            idCell = xFish + yFish * gridWidth;

            // Initialization of hsi of cell(idCell)
            grid[idCell] = new Cell(idCell, hsiMean);
        }
        scanner.close(); // close the file
    }

    /**
     * Process which defines the value of the HSI for one cell
     *
     * @param indexCell Ind of the cell
     * @param hsiCell  HSI value
     * @return /
     */
    public void setCell(int indexCell, double hsiCell) {
        grid[indexCell] = new Cell(indexCell, hsiCell);
    }

    /**
     * Process which  return the current grid during the simulation
     *
     * @param /
     * @return Grid Grid of the simulation
     */
    public Grid getGrid() {
        return this;
    }

    /**
     * Process which add an individual to aquaNism group
     *
     * @param ind the individual to add
     * @param group Group of individual
     * @return /
     */
    public void addAquaNism(Individual ind, AquaNismsGroup<?, ?> group) {
        super.addAquaNism(ind, group);
        ind.getPosition().addFish((Fish) ind);
    }

    /**
     * Process which move an individual
     *
     * @param ind the individual to move
     * @param group Group to which belongs the individual
     * @param dest Cellule de destination de l'individu
     * @return / Cell of destination of the individual
     */
    public void moveAquaNism(Individual ind, AquaNismsGroup<?, ?> group, Cell dest) {
        super.moveAquaNism(ind, group, dest);
        this.removeAquaNism(ind, group);
        dest.addFish((Fish) ind);
    }

    /**
     * Process which delete an individual
     *
     * @param ind the individual to remove
     * @param group Group to which belongs the individual
     * @return /
     */
    public void removeAquaNism(Individual ind, AquaNismsGroup<?, ?> group) {
        super.removeAquaNism(ind, group);
        ind.getPosition().removeFish((Fish) ind);
    }

    /**
     * Calculation of accessible cells (included in the lake)
     *
     * @param position ID of initial cell
     * @return neighbours List of accessible cells
     */
    public List<Cell> getNeighbours(Cell position) {

        List<Cell> neighbours = new ArrayList<Cell>();
        int[][] listeCoord;
        int xFish = 0, yFish = 0;

        // Calculation of coordinates (x, y) from Id cell
        yFish = (position.getIndex() / gridWidth);
        xFish = (position.getIndex() - (yFish * gridWidth));
        

        // Determining the list of cells for a given distance that depends on the phase of the day and the season
        switch (Time.getSeason()) {
            case "PRINTEMPS":
                switch (Time.getPhaseOfDay()) {
                    case "AUBE":
                        listeCoord = AreaMovement.areaPrinAube;
                        break;
                    case "JOUR":
                        listeCoord = AreaMovement.areaPrinJour;
                        break;
                    case "NUIT":
                        listeCoord = AreaMovement.areaPrinNuit;
                        break;
                    case "CREP":
                        listeCoord = AreaMovement.areaPrinCrep;
                        break;
                    default:
                        System.out.println("Erreur lecture de la Phase du Jour au Printemps");
                        listeCoord = AreaMovement.areaPrinJour;
                        break;
                }
                break;

            case "ETE":
                switch (Time.getPhaseOfDay()) {
                    case "AUBE":
                        listeCoord = AreaMovement.areaEteAube;
                        break;
                    case "JOUR":
                        listeCoord = AreaMovement.areaEteJour;
                        break;
                    case "NUIT":
                        listeCoord = AreaMovement.areaEteNuit;
                        break;
                    case "CREP":
                        listeCoord = AreaMovement.areaEteCrep;
                        break;
                    default:
                        System.out.println("Erreur lecture de la Phase du Jour en Ete");
                        listeCoord = AreaMovement.areaEteJour;
                        break;
                }
                break;

            case "AUTOMNE":
                switch (Time.getPhaseOfDay()) {
                    case "AUBE":
                        listeCoord = AreaMovement.areaAutAube;
                        break;
                    case "JOUR":
                        listeCoord = AreaMovement.areaAutJour;
                        break;
                    case "NUIT":
                        listeCoord = AreaMovement.areaAutNuit;
                        break;
                    case "CREP":
                        listeCoord = AreaMovement.areaAutCrep;
                        break;
                    default:
                        System.out.println("Erreur lecture de la Phase du Jour en Automne");
                        listeCoord = AreaMovement.areaAutJour;
                        break;
                }
                break;

            case "HIVER":
                switch (Time.getPhaseOfDay()) {
                    case "AUBE":
                        listeCoord = AreaMovement.areaHivAube;
                        break;
                    case "JOUR":
                        listeCoord = AreaMovement.areaHivJour;
                        break;
                    case "NUIT":
                        listeCoord = AreaMovement.areaHivNuit;
                        break;
                    case "CREP":
                        listeCoord = AreaMovement.areaHivCrep;
                        break;
                    default:
                        System.out.println("Erreur lecture de la Phase du Jour en Hiver");
                        listeCoord = AreaMovement.areaHivJour;
                        break;
                }
                break;

            default:
                System.out.println("Erreur lecture de la Saison");
                switch (Time.getPhaseOfDay()) {
                    case "AUBE":
                        listeCoord = AreaMovement.areaPrinAube;
                        break;
                    case "JOUR":
                        listeCoord = AreaMovement.areaPrinJour;
                        break;
                    case "NUIT":
                        listeCoord = AreaMovement.areaPrinNuit;
                        break;
                    case "CREP":
                        listeCoord = AreaMovement.areaPrinCrep;
                        break;
                    default:
                        System.out.println("Plus : Erreur lecture de la Phase du Jour");
                        listeCoord = AreaMovement.areaPrinJour;
                        break;
                }
                break;
        }
            
        //Calculation of the cells included in the lake
        boolean stop = false;
        Cell cellule = null;
        for (int cpt = 0; cpt < listeCoord[0].length; cpt++) {

            if (listeCoord[0][cpt] == 0 && listeCoord[1][cpt] == 0 && stop == true) {
                break;
            }
            if (listeCoord[0][cpt] == 0 && listeCoord[1][cpt] == 0) {
                stop = true;
            }

            //Calculation of the cell index from the coordinates(x,y)
            int newCell = xFish + listeCoord[0][cpt] + (yFish + listeCoord[1][cpt]) * gridWidth;
            
            // Test if the index calculates belongs to the total grid
            if (newCell >= 0 && newCell <= ((gridWidth*gridHeight)-2)) {
                cellule = getCell(newCell);
                
                // If the calculated index is included in the lake, add it to the list
                if (cellule.getHabitatQuality() >= 0) {
                    neighbours.add(cellule);
                }
            }
        }
        
        return neighbours;
    }

    private int computeIndex(int xFish, int[][] listeCoord, int cpt, int yFish) {
        return xFish + listeCoord[0][cpt] + (yFish + listeCoord[1][cpt]) * gridWidth;
    }
}

