/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake;

import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.simaqualife.kernel.AquaNism;
import fr.cemagref.simaqualife.pilot.Pilot;

public abstract class Individual extends AquaNism<Cell, Grid> implements Comparable<Individual> {

	/**
	 * <code>age</code> age of the individual in months
	 */
	protected int age;
        
	/**
	 * <code>weight</code> weight of the individual in grams
	 */
	@Observable(description = "weight (g)")
	protected double weight;

	/**
	 * <code>sexe</code> Sex of individual (0: Female, 1: Male)
	 */
	protected boolean sex;

	public Individual (Pilot pilot, Cell position) {
		super(pilot, position);
		this.age = 0;
	}

	public Individual (Pilot pilot, Cell position, double weight) {
		this(pilot, position);
		this.weight = weight;
	}

	public final double getWeight () {
		return weight;
	}

	public final void setWeight (double weight) {
		this.weight = weight;
	}

	public final int getAge () {
		return age;
	}

	public final void incAge () {
		age++;
	}

	public int compareTo (Individual ind) {
		return (int) Math.signum(this.weight - ind.weight);
	}

}
