/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake.environment;

import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;

import simfishlake.fishes.Fish;
import simfishlake.fishes.FishesGroup;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

/**
 *		Class that reads the file containing the sunrise and sunset times
 *		These times are stored at initialization in a table for faster access
 *		@author Guillaume GARBAY and Lauriane MASSARDIER
 *		@version 1.0
 */
public class SunsetAndSunriseTime extends AquaNismsGroupProcess<Fish,FishesGroup> {
 // Arrays containing the times of sunrise and sunset and the phases of the day according to the date
	public static int sunriseAndSunsetTime[][][] = new int[2][32][13] ;	//	[0=sunrise, 1=sunset]	[day]	[month]	
	public static String phaseOfDay[][][] = new String[32][13][25];	//	[day]	[month]	[hour]

	/**
	 *		Read the file containing the sunrise and sunset times
	 *		Fill the arrays sunriseAndSunsetTime and phaseOfDay
	 *		@param object All of individuals
	 *		@return /
	 */
	@Override
	public void doProcess (FishesGroup object) {
		
		// Variables that retrieve the values read from the file
		String temp[] = null, dateCompleteSunrise[] = null, dateCompleteSunset[] = null;
		String day = null, month = null, sunriseTime = null, sunsetTime = null;
	
		// File containing the times of sunrise and sunset
		String filePath = "data/input/thehoursofsun.csv";    	
		Scanner scanner;
		try {
			scanner = new Scanner(new File(filePath));
			String line = scanner.nextLine();
			
			//loop on the line
			while (scanner.hasNextLine()) {
				
				line = scanner.nextLine();	// Read the line
				temp = line.split(";");		// Cut the line

				// Determination of sunrise time
				dateCompleteSunrise = temp[0].split("[/: ]+");
				dateCompleteSunset = temp[1].split("[/: ]+");
					// Calculate date
				day = dateCompleteSunrise[0];
				month = dateCompleteSunset[1];
					// Sunrise
				sunriseTime = dateCompleteSunrise[3];
					// Sunset
				sunsetTime = dateCompleteSunset[3];
				
				
				sunriseAndSunsetTime[0][Integer.parseInt(day)][Integer.parseInt(month)] = Integer.parseInt(sunriseTime);
				sunriseAndSunsetTime[1][Integer.parseInt(day)][Integer.parseInt(month)] = Integer.parseInt(sunsetTime);				
			}
			scanner.close(); // close file 
		} catch (FileNotFoundException e) {
			// Auto-generated catch block, error of read file 
			e.printStackTrace();
		}
		
		// Filling of the table which determines the phase of the day with the times read in the file
		for (int m=1; m<13 ;m++)
			for (int j=1; j<32; j++) 
				for (int h=0; h<25; h++) {
					if		(h > sunriseAndSunsetTime[0][j][m] -2 & h < sunriseAndSunsetTime[0][j][m] +2)	phaseOfDay[j][m][h] = "AUBE";
					else if (h > sunriseAndSunsetTime[0][j][m] +1 & h < sunriseAndSunsetTime[1][j][m] -1)	phaseOfDay[j][m][h] = "JOUR";
					else if (h > sunriseAndSunsetTime[1][j][m] -2 & h < sunriseAndSunsetTime[1][j][m] +2)	phaseOfDay[j][m][h] = "CREP";
					else	phaseOfDay[j][m][h] = "NUIT";
				}			
	}	
	
	/**
	 *		Returns the phase of the day according to a date (day + month + hour)
	 *		@param day Integer containing the day
	 *		@param month Integer containing the month
	 *		@param hour Integer containing the jour
	 *		@return phaseOfDay[day][month][hour] String containing the phase of the day
	 */
	public static String getPhase (int day, int month, int hour) {
		return phaseOfDay[day][month][hour];
	}
	
	/**
	 *		Returns the time to wake up or sleep based on a date (day + month)
	 *		@param sunriseSunset Integer or boolean 0=sunrise, 1=sunset		 
	 *		@param day Integer containing the day
	 *		@param month Integer containing the month
	 *		@return sunriseAndSunsetTime[0=sunrise, 1=sunset][day][month] Integer representing the time of sunrise or sunset
	 */
	public int getTimeTable (int sunriseSunset, int day, int month) {
		return sunriseAndSunsetTime[sunriseSunset][day][month] ;
	}
	
}
