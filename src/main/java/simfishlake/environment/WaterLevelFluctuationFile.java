/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake.environment;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;

import simfishlake.fishes.Fish;
import simfishlake.fishes.FishesGroup;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

/**
 *		Class wich reads the file containing the water level fluctuation file of the lake 
 *		These values are put during initialization in a tab for a faster access
 *		@author Guillaume GARBAY and Lauriane MASSARDIER
 *		@version 1.0
 */
public class WaterLevelFluctuationFile extends AquaNismsGroupProcess<Fish,FishesGroup> {
// Array containing the water level in function of the date
	public static String dateWaterLevel[][][] = new String[32][13][24];	//	[day] [month] [hour]
	
	/**
	 *		Read the file containing the water level fluctuation of the lake
	 *		Fill the array dateWaterLevel
	 *		@param object all the individual
	 *		@return /
	 */
	@Override
	public void doProcess (FishesGroup object) {
		
		// file containing the water level fluctuation for each jour in the year
    	String file = "data/input/CoteCorrige_01012012_30042014.txt";
    	try {
    		Scanner scanner = new Scanner(new File(file));
    		String line = scanner.nextLine();
	    	
	    	// loop on the line
	    	while (scanner.hasNextLine()) {
	    		
	    		line = scanner.nextLine();				// read the line
	    		String temp[] = line.split("[/:\t ]+");	

	    		// Initialization of the array containing water level for each date(monnth, day,hour)
	    		dateWaterLevel[Integer.parseInt(temp[0])] [Integer.parseInt(temp[1])] [Integer.parseInt(temp[3])] = 
	    									temp[6].substring(0, temp[6].length()-1).replace(".", "_");	
	    	}
	       	scanner.close(); // close file
		} catch (FileNotFoundException e) {
			// Auto-generated catch block, erreur de lecture du fichier
			e.printStackTrace();
		}
	}	
	
	/**
	 *		return water level function of the date
	 *		@param day Integer containing the day
	 *		@param month Integer containing the month
	 *		@param hour Integer containing the hour
	 *		@return dateWaterLevel[day][month+1][hour] 
	 */
	public static String calculMarnage (int day, int month, int hour) {
		return dateWaterLevel[day][month][hour];
	}
}