/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake.environment;

import simfishlake.fishes.Fish;
import simfishlake.fishes.FishesGroup;
import simfishlake.environment.SunsetAndSunriseTime;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

/**
 *		Class that manages the simulation time
 *		@author Guillaume GARBAY and Lauriane MASSARDIER
 *		@version 1.0
 */
public class Time extends AquaNismsGroupProcess<Fish,FishesGroup> {

		// Variables representing the date of the simulation
		public static enum Season {PRINTEMPS, ETE, AUTOMNE, HIVER};
		public static enum Month {MoisZero, JANVIER, FEVRIER, MARS, AVRIL, MAI, JUIN, JUILLET, AOUT, SEPTEMBRE, OCTOBRE, NOVEMBRE, DECEMBRE};
		public static int month = 1, day = 1, dayOfMonth =1, season = 1, year = 2012, phaseOfDay = 0, hour = 0;
		public static String PhaseOfDay;
		public static long nbrIter = 0;
		
		/**
		 *		Determine the date of the simulation according to the number of steps of the simulation
		 *		Calculates time, day, phase of the day, month, season and year
		 *		!! 1 <= day <= 365	;	1 <= dayOfMonth <= 30,31 !!
		 *		@param group all of individuals
		 *		@return /
		 */
		@Override
		public void doProcess (FishesGroup group) {
			
			// Number of time step
			nbrIter = group.getPilot().getCurrentTime();
                        		
			// Hourly time step
			hour = (int) nbrIter % 24;	
			day = (int) Math.ceil((nbrIter+1) / 24.);
                        			 
                       // day=day+group.getFirstDay();// fisrt day allows  to shift the first day of the simulation, for example if fisrDay=0 begin in 1 Jan, if firstDay=264 begin in 22 Sep...
                                              
                        
			// Reset the number of days per year if the year is over
			if (day>365) {
				year = year + 1;
				day = day - 365;
			}
			
			// Determination of the day in the month
			if (hour == 0)
				dayOfMonth = dayOfMonth + 1;
			
			//Reset dayOfMonth
			if ((month == 1 | month == 3 | month == 5 | month == 7 | month == 8 | month == 10 | month == 12) & dayOfMonth == 32)	dayOfMonth = 1;
			else if ((month == 4 | month == 6 | month == 9 | month == 11) & dayOfMonth == 31) dayOfMonth = 1;
			else if  (month == 2 & dayOfMonth == 29) dayOfMonth = 1;
			
			// Calculation of the month, the season and the phase of the day
			calculationMonth();
			calculationSeason();
			PhaseOfDay = SunsetAndSunriseTime.phaseOfDay[dayOfMonth][month][hour];
                }

		/**
		 *		Determine the month according to the day per year
		 *		@param / 
		 *		@return /
		 */
		public void calculationMonth () {
			if	(day <= 31)	day = 1; // Jan
			else if (day <= 59)	day = 2; // Fev
			else if (day <= 90)	day = 3; // March
			else if (day <= 120)	day = 4;// April
			else if (day <= 151)	day = 5; // May
			else if (day <= 181)	day = 6; // Jun
			else if (day <= 212)	day = 7; // Juil
			else if (day <= 243)	day = 8; // Aug
			else if (day <= 273)	day = 9; // Sept
			else if (day <= 304)	day = 10; // Oct
			else if (day <= 334)	day = 11; // Nov
			else if (day <= 365)	day = 12; // Dec
			else	day = 0;
		}
		
		/**
		 *		Determine the season according to the month and the day of the month
		 *		@param / 
		 *		@return /
		 */
		public void calculationSeason () {
			if (month == 1)season = 3;
			if (month == 3 & dayOfMonth >= 20)	season = 0;
			else if (month == 6 & dayOfMonth >= 20)	season = 1;
			else if (month == 9 & dayOfMonth >= 22)	season = 2;
			else if (month == 12 & dayOfMonth >= 21)season = 3;
		}
		
		/**
		 *		Return the phase of the day
		 *		@param / 
		 *		@return PhaseOfDay String containing the phase of the day
		 */
		public static String getPhaseOfDay () {					
			return PhaseOfDay;
		}
		
		/**
		 *		Return the month		 
                 *		@param / 
		 *		@return Month[i] String containing the month
		 */
		public static String getMonth () {					
			return Month.values()[month].toString();
		}

		/**
		 *		Return the season
		 *		@param / 
		 *		@return Season[i] String containing the season
		 */
		public static String getSeason () {
			return Season.values()[season].toString();
		}
}
