/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake.environment;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;
import simfishlake.fishes.Fish;
import simfishlake.fishes.FishesGroup;

/**
 *		Class which reads the file containing the distances traveled by the individuals,
 *		according to the season and the phase of the day.
 *		Three distances are available (min, avg, max), choice of the distance used in the file fishLight.xml
 *		@author Guillaume GARBAY and Lauriane MASSARDIER
 *		@version 1.0
 */
public class AreaMovement extends AquaNismsGroupProcess<Fish, FishesGroup> {

	private int distMin = 0, distAvg = 0, distMax = 0, std = 0;//, choiceDist = 0;
	private String season = null, phaseOfDay = null;

	// Array of integer containing accessible cell coordinates
	public static int[][] areaPrinAube, areaPrinJour, areaPrinNuit, areaPrinCrep;
	public static int[][] areaEteAube,  areaEteJour,  areaEteNuit,  areaEteCrep;
	public static int[][] areaAutAube,  areaAutJour,  areaAutNuit,  areaAutCrep;
	public static int[][] areaHivAube,  areaHivJour,  areaHivNuit,  areaHivCrep;
	
	/**
	 *		Read the file containing the travelled distances in function of the season and the phase of the day.
	 *		Fill the arrays containing coordinates of accessible cells.
	 *		@param object all of individuals
	 *		@return / 
	 */
	@Override
	public void doProcess (FishesGroup group) {
            
            String filePath = "data/input/HourlyCartesianDistance_";
            // Read file containing the hourly distances in function of the species
            switch (group.getSpecies()){ 
                case 0:// Species 0 : Pike
                    filePath = filePath+"Pike.txt";   
                    break;
                case 1:// Species 1 : Perch
                    filePath = filePath+"Perch.txt";   
                    break;
                case 2:// Species 2 : Pikeperch
                    filePath = filePath+"Pikeperch.txt";   
                    break;
            }
            
		Scanner scanner;
		try {
			scanner = new Scanner(new File(filePath));
			String line = scanner.nextLine();
			
			// loop on line
			while (scanner.hasNextLine()) {
				line = scanner.nextLine();		
				
				// Cut line : season ; phase of day ; distance : minimun, average, maximum and std
				StringTokenizer sLigne = new StringTokenizer (line);
				if (sLigne.hasMoreTokens())
					season = sLigne.nextToken();
				if (sLigne.hasMoreTokens())
					phaseOfDay = sLigne.nextToken();
				if (sLigne.hasMoreTokens())
					distMin = Integer.parseInt(sLigne.nextToken());
				if (sLigne.hasMoreTokens())
					distAvg = Integer.parseInt(sLigne.nextToken());
				if (sLigne.hasMoreTokens())
					distMax = Integer.parseInt(sLigne.nextToken());
				if (sLigne.hasMoreTokens())
					std = Integer.parseInt(sLigne.nextToken());

				// fill array correspondong to current line 
				if (season.equals("PRINTEMPS")) {
					if 	(phaseOfDay.equals("AUBE"))	areaPrinAube = choiceDistArea (group);
					else if (phaseOfDay.equals("JOUR"))	areaPrinJour = choiceDistArea (group);
					else if (phaseOfDay.equals("CREP"))	areaPrinCrep = choiceDistArea (group);
					else if (phaseOfDay.equals("NUIT"))	areaPrinNuit = choiceDistArea (group);
					
				} else if (season.equals("ETE")) {
					if	(phaseOfDay.equals("AUBE"))	areaEteAube = choiceDistArea (group);
					else if (phaseOfDay.equals("JOUR"))	areaEteJour = choiceDistArea (group);
					else if (phaseOfDay.equals("CREP"))	areaEteCrep = choiceDistArea (group);
					else if (phaseOfDay.equals("NUIT"))	areaEteNuit = choiceDistArea (group);
			
				} else if (season.equals("AUTOMNE")) {
					if	(phaseOfDay.equals("AUBE"))	areaAutAube = choiceDistArea (group);
					else if (phaseOfDay.equals("JOUR"))	areaAutJour = choiceDistArea (group);
					else if (phaseOfDay.equals("CREP"))	areaAutCrep = choiceDistArea (group);
					else if (phaseOfDay.equals("NUIT"))	areaAutNuit = choiceDistArea (group);
				
				} else if (season.equals("HIVER")) {
					if	(phaseOfDay.equals("AUBE"))	areaHivAube = choiceDistArea (group);
					else if (phaseOfDay.equals("JOUR"))	areaHivJour = choiceDistArea (group);
					else if (phaseOfDay.equals("CREP"))	areaHivCrep = choiceDistArea (group);
					else if (phaseOfDay.equals("NUIT"))	areaHivNuit = choiceDistArea (group);
				}		
			}
		scanner.close(); // Close file 
		} catch (FileNotFoundException e) {
			// Auto-generated catch block, error of read
			e.printStackTrace();
		}
            
	}
	
	/**
	 *		Determine the distance that will be used to calculate moving areas
	 *		@param / 
	 *		@return temp Fill the arrays containing coordinates of accessible cells.
	 */
	public int[][] choiceDistArea (FishesGroup group) {
		int[][] temp = null;
		if 	(group.getChoiceDist() == 0) 	temp = calculationArea (distMin);
		else if (group.getChoiceDist() == 1)	temp = calculationArea (distAvg);
		else if (group.getChoiceDist() == 2)	temp = calculationArea (distMax);
                return temp;
	}
	
    /**
     * Determine the coordinates of the circle of accessible cells for a given distance
     *
     * @param distance Integer representing the distance
     * @return area Fill the arrays containing coordinates of accessible cells.
     * 
     */
    public int[][] calculationArea(int distance) {

        List<List<Integer>> area = new ArrayList<List<Integer>>();
        area.add(new ArrayList<Integer>());
        area.add(new ArrayList<Integer>());
        int xi = 0, yi = 0, cpt = 0;

        // Convert distance to cell number
        int distCell = (int) Math.round(distance / 10.);

        // Calculation of the coordinates (x, y) of the cells for a given distance		
        for (xi = -distCell; xi <= distCell; xi++) {
            yi = (int) Math.round(Math.sqrt(distCell * distCell - xi * xi));
            for (int j = -yi; j <= yi; j++) {
                area.get(0).add(xi);
                area.get(1).add(j);
            }
        }
        int[][] result = new int[2][area.get(0).size()];
        for (int i = 0; i < area.size(); i++) {
            for (int j = 0; j < area.get(i).size(); j++) {
                result[i][j] = area.get(i).get(j);
            }
        }

        return result;
    }
	
	/**
	 *		Return min distance
	 *		@param / 
	 *		@return distMin Integer containing min distance
	 */
	public int getDistMin () {
		return distMin;
	}
	
	/**
	 *		Return average distance
	 *		@param / 
	 *		@return distMoy Integer containing avg distance
	 */
	public int getDistAvg () {
		return distAvg;
	}
	
	/**
	 *		Return maximal distance
	 *		@param / 
	 *		@return distMax Integer containing max distance
	 */
	public int getDistMax () {
		return distMax;
	}
	
	/**
	 *		Return standard deviation 
	 *		@param / 
	 *		@return std Integer containing std distance
	 */
	public int getDistStd () {
		return std;
	}

	/**
	 *		Return the season
	 *		@param / 
	 *		@return saison String containing season
	 */
	public String getSeason () {
		return season;
	}
	
	/**
	 *		Return the phase of the day
	 *		@param / 
	 *		@return phaseJour String containing phase of the day
	 */
	public String getPhaseOfDay () {
		return phaseOfDay;
	}

}
