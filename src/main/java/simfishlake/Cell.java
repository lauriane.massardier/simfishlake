/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake;

import java.util.List;
import java.util.ArrayList;
import java.lang.Comparable;

import simfishlake.fishes.Fish;

import fr.cemagref.simaqualife.extensions.spatial2D.IndexedCell;
import java.util.Comparator;
import java.util.function.ToDoubleFunction;

public class Cell extends IndexedCell  {

    /**
     * <code>habitatQuality</code> The quality of this place (fish point of
     * view). A high value means that preys are vulnerable for the fishes
     */
    private double habitatQuality;

    /**
     * <code>pikes</code> The list of fishes in this place
     */
    private transient List<Fish> fishes;

    public Cell (int index, double habitatQuality) {
        super(index);
        this.habitatQuality = habitatQuality;
        fishes = new ArrayList<Fish>();
    }

    public List<Fish> getFishes () {
        return fishes;
    }
    
    public List<Fish> addFishesCell (Cell cell) {
        return fishes;
    }

    
    public boolean addFish (Fish fish) {
        return fishes.add(fish);
    }
    
    public boolean removeFish (Fish fish) {
        return fishes.remove(fish);
    }
  
    public double getHabitatQuality () {
        return habitatQuality;
    }
    
}
