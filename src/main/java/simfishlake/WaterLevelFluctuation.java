/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake;

import java.io.File;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.io.FileNotFoundException;

import simfishlake.fishes.Fish;
import simfishlake.fishes.FishesGroup;
import simfishlake.environment.Time;
import simfishlake.environment.WaterLevelFluctuationFile;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
/**
 *		Class which updates the accessible cells of the lake, as well as their HSI
 *		according to the hourly water level given by the WaterLevelFluctuationFile class
 *		@author Guillaume GARBAY and Lauriane MASSARDIER
 *		@version 1.0
 */
public class WaterLevelFluctuation extends AquaNismsGroupProcess<Fish,FishesGroup>{
 public static String waterLevelOld = null, waterLevelNew = null;

	/**
	 *		Test if the lake rating is changed, if yes
	 *		Update of grid and HSI of each cell
	 *		@param group all individuals
	 *		@return /
	 */	
	@Override
	public void doProcess(FishesGroup group) {
		// Grid recovery
		Grid grid = (Grid) pilot.getAquaticWorld().getEnvironment();
		grid = grid.getGrid();
		// Calculation of water level
		waterLevelNew = WaterLevelFluctuationFile.dateWaterLevel[Time.dayOfMonth][Time.month][Time.hour];
		// update the grid only if the water level is changed
		if (!waterLevelNew.equals(waterLevelOld)) {
                    synchronized (grid) { // synchronization to be sure that graphical observer read synchronized values
                        updateGrid(grid,group);
                    }
                }
	}

	/**
	 *		Updating the grid, increase or decrease the number of accessible cells
	 *		Change the HSI of each cell
	 *		@param grid Represents lake and non-accessible cells
         *              @param group
	 *		@return /
	 */
    public void updateGrid (Grid grid,FishesGroup group) {
		
    	StringTokenizer sLigne;
	    @SuppressWarnings("unused")
		double hsiStd = 0, hsiMean = 0;
		int idCell = 0, yFish = 0, xFish = 0;
		waterLevelOld = waterLevelNew;
                
		
	    // Initialization of all grid with hsi = -1
	    for (int cptCell = 0; cptCell < (grid.getGridWidth() * grid.getGridHeight() - 1); cptCell++)
	    	grid.setCell(cptCell, -1);
	    
	    // Read file containing the HSIs of all cells
             String filePath = "data/input/HSI/hsi_";
            // Read file containing the hourly distances in function of the species
            switch (group.getSpecies()){ 
                case 0:// Species 0 : Pike
                    filePath = filePath+"BRO"+ Time.getSeason() + waterLevelNew + ".txt";   
                    break;
                case 1:// Species 1 : Perch
                    filePath = filePath+"PER"+ Time.getSeason() + waterLevelNew + ".txt";   
                    break;
                case 2:// Species 2 : Pikeperch
                    filePath = filePath+"SAN"+ Time.getSeason() + waterLevelNew + ".txt";    
                    break;
            }
	    
            System.out.println(filePath);
	    Scanner scanner;
		try {
			scanner = new Scanner(new File(filePath));
			String line = scanner.nextLine();
			
		   	// loop on the line
		   	while (scanner.hasNextLine()) {
		   		line = scanner.nextLine();
		   		
		   		// Cut line : id, hsiStd, hsiMean
		   		sLigne = new StringTokenizer (line);
		   		if (sLigne.hasMoreTokens())
		   			idCell = Integer.parseInt(sLigne.nextToken())-1;
		   		if (sLigne.hasMoreTokens())
		    	   	hsiStd = Double.parseDouble(sLigne.nextToken());
		   		if (sLigne.hasMoreTokens())
		    	   	hsiMean = Double.parseDouble(sLigne.nextToken());
		   		
		   		// Conversion idCell in coordinates (x, y) 
		   		// (x, y) with id of cells from 0 to n-1
		   		yFish = (int) Math.floor(idCell / grid.getGridWidth());
                                xFish = (int) idCell - (yFish * grid.getGridWidth());
		       	
		       	// Inversion of coordinates in Y 
		       	yFish = (grid.getGridHeight()-1) - yFish ;
		       	
		       	// Inversion of coordinates (x, y) in idCell 
		       	idCell = xFish + yFish * grid.getGridWidth();
		       	
		       	// Initialization of hsi of the cell(idCell)
		       	grid.setCell(idCell, hsiMean);
		   	}
		   	scanner.close(); // Close the file 
		} catch (FileNotFoundException e) {
			// Auto-generated catch block, error of file
			e.printStackTrace();
		}
	}
}

