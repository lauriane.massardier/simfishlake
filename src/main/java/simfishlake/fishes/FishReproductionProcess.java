/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake.fishes;

import java.util.ArrayList;
import java.util.List;
import umontreal.iro.lecuyer.probdist.PoissonDist;
import umontreal.iro.lecuyer.randvar.PoissonGen;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 * @author patrick.lambert
 *
 */
public class FishReproductionProcess extends AquaNismsGroupProcess<Fish,FishesGroup> {
    /**
	 * <code>ageAtFirstReproduction</code> age at first reproduction (in year)
	 */
	private int ageAtFirstReproduction = 5;
	
	/**
	 * <code>fishFertility</code> mean number of offsprings per fish
	 */
	private double fishFertility = 1.1;
	
	transient private PoissonGen poissonGen;

	public void initTransientParameters (Pilot pilot) {
		poissonGen = new PoissonGen(pilot.getRandomStream(), new PoissonDist(fishFertility));
		
	}
	
	public void doProcess (FishesGroup group) {
		System.out.println("FishReproductionProcess");
		
		int offspring;
		if (1+ ((group.getPilot().getCurrentTime()-1) % 12) == group.getMonthOfBirth()){
			List<Fish> offsprings = new ArrayList<Fish>();
			List<Fish> deadSpwaners = new ArrayList<Fish>();
			for (Fish fish: group.getAquaNismsList()){
				if (Math.floor((double) fish.getAge()/12.) >= ageAtFirstReproduction){
					// number of offspring
					offspring = poissonGen.nextInt();

					System.out.println("  offspring #: "+ offspring);
					for (int i=0; i<offspring ;i++)
						offsprings.add(new Fish(group.getPilot(), fish.getPosition()));
					// die after reproduction
					//if (offspring > 0) // only if reproduce
						deadSpwaners.add(fish);		
				}	
			}
	    // update the fishesGroup
		for (Fish fish: deadSpwaners){
			group.getAquaNismsList().remove(fish);
			//ASK WHY
			fish.getPosition().removeFish(fish);
		}

		for (Fish fish: offsprings)
			group.addAquaNism(fish);
		}
	}
	
}
