/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake.fishes;

import java.util.Comparator;
import umontreal.iro.lecuyer.probdist.UniformDist;
import umontreal.iro.lecuyer.randvar.UniformGen;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.processes.LoopAquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 * @author patrick.lambert
 * 
 */
public class FishHuntProcess extends
LoopAquaNismsGroupProcess<Fish, AquaNismsGroup<Fish, ?>>{
 /**
	 * <code>satiety</code> ratio of the pike weigth which stops eating
	 * preys
	 */
	@SuppressWarnings("unused")
	private double satiety = 0.10;

	@SuppressWarnings("unused")
	transient private UniformGen uniformGen;

	@Override
	protected void doProcess (Fish fish, AquaNismsGroup<Fish, ?> group) {
		
		System.out.println("FishHuntProcess");
		
//		Cell fishCell = fish.getPosition();
/*		if (fishCell.getPreys().size() > 0) {
			//System.out.print(" " +  fishCell.getPreys().size() +" preys available in cell ");
			//System.out.print("("+fishCell.getX()+","+fishCell.getY()+")");
			
			// sort the preys list according to the weigth
			List<Prey> sortedPreys = fishCell.getPreys();
			Collections.sort(sortedPreys);

			double ratio = fish.getIngestedFood()/fish.getWeight();
			int idx = 0;
			int idxMax =  sortedPreys.size();
			int target=0;
			int eaten=0;
			while ((fish.getIngestedFood()/fish.getWeight() < satiety) && (idx < idxMax)) {

				// the probability for a prey to be eaten depends 
				// on the habitat quality in the cell
				if (uniformGen.nextDouble() < fishCell.getHabitatQuality()){
					// eat the prey
					Prey eatenPrey = sortedPreys.get(target);
					fish.addIngestedFood(eatenPrey.getWeight());

					// and the prey dies
					
					((PreysGroup)group.getPilot().getAquaticWorld().getAquaNismsGroupsList().get(0)).removeAquaNism(eatenPrey);
					//this.interAquanism(eatenPrey);
					fishCell.removePrey(eatenPrey);
					eaten ++;
				}
				else
					target ++;

				idx++;
				ratio = fish.getIngestedFood()/pike.getWeight();
			}
			ratio= ((double) Math.round(100*ratio))/100;
			//System.out.println(", " + eaten + " preys eaten ("+ ratio +" ), still " +  fishCell.getPreys().size());
		}	*/

	}

	@InitTransientParameters
	public void initTransientParameters (Pilot pilot) {
		sorted = true;
		comparator = new Comparator<Fish>() {
			public int compare(Fish o1, Fish o2) {
				return (int) Math.signum(o2.getWeight() - o1.getWeight());
			}
		};

		uniformGen = new UniformGen(pilot.getRandomStream(), new UniformDist());
	}

}