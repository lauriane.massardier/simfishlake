/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake.fishes;

import simfishlake.Grid;
import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.Processes;
import fr.cemagref.simaqualife.pilot.Pilot;

public class FishesGroup extends AquaNismsGroup<Fish,Grid>  {
    	
        /**
	 * <code>species</code> choice the species: 0=>pike, 1=>perch and 2=>pikeperch
	 */
	private int species;
    
	/**
	 * <code>weightAtAgeThreshold</code> threshold of the ratio of  weight (in g) by age (in year)
	 */
	private double weightAtAgeThreshold;
	
	
	/**
	 * <code>monthOfBirth</code> month of reproduction
	 */
	private int monthOfBirth;
	
        /**
	 * <code>initialNumberOfFishes</code> number of fishes during the simulation
	 */
        private int initialNumberOfFishes;
        
        private int day;
        private int month;
        private int year;
        private int season; 
        
        /**
	 * <code>firstDay</code> define the first day especially if the first day is not January 1st. 
         * In fishlight.xml you have to set the departure day as an integer, for example to start in autumn on September 22nd, firstDay = 264, or July 1st firstday = 212 ...
	 */
        private int firstDay;
        
         /**
	 * <code>timeStep</code> define the simulation time, one year = 8760, one season = 2159
	 */
        private int timeStep;
        
        /**
	 * <code>choiceDist</code> Choose the hourly travelled distance; 0 => distMin;	1 => distAvg;	2 => distMax
	 */
        private int choiceDist;
        
         /**
	 * <code>movementChoice</code> Choose the type of movement; Movement type 0 => RandomWalk;	1 => ROfNBetterHSI each individual moves in a random cell belong to the n1 better HSI cells included in its Activity radius; 2 => BestHSIAmongNR  each individual moves in the best HSI value belong to n2 random cells of the those included in Activity Radius;
	 */
        private int movementChoice;
        
        /**
	 * <code>percentOfCells</code> define the percent of cells needed to
	 */
        private int percentOfCells;
        
        
	
	@Observable(description = "Fishes biomass (g)")
	private transient double fishesBiomass;
	
	@Observable(description = "Fishes number")
	private transient int fishesNumber;
	
	
	public FishesGroup (Pilot pilot, Grid arg0, Processes arg1) {
		super(pilot, arg0, arg1);
	}
	
	public int calculateFishesNumber () {
		fishesNumber = this.getAquaNismsList().size();
		
		return fishesNumber;
	}

	public double calculateFishesBiomass () {
		fishesBiomass =0.;
			for(Fish fish : this.getAquaNismsList())
				fishesBiomass += fish.getWeight();
		return fishesBiomass;
	}

        
	public int getMonthOfBirth () {
		return monthOfBirth;
	}

	public double getWeightAtAgeThreshold () {
		return weightAtAgeThreshold;
	}
        
        public int getSpecies(){
            return species;
        }
        
        public int getInitialNumberOfFishes(){
            return initialNumberOfFishes;
        }
        
        public int getDay(){
            return day;
        }
         
        public int getMonth(){
            return month;
        }
        
        public int getYear(){
            return year;
        }
        
         public int getSeason(){
            return season;
        }
        
        public int getFirstDay(){
            return firstDay;
        }
        
        public int getTimeStep(){
            return timeStep;
        }
        
        public int getChoiceDist() {
            return choiceDist;
        }
        
        public int getMovementChoice(){
            return movementChoice;
        }
        
        public int getPercentOfCells(){
            return percentOfCells;
        }
}
