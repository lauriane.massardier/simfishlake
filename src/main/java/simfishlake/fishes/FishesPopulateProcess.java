/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake.fishes;

import simfishlake.Cell;
import umontreal.iro.lecuyer.randvar.UniformGen;
import umontreal.iro.lecuyer.probdist.UniformDist;
import fr.cemagref.simaqualife.pilot.Pilot;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;

/**
 *		Classes that initializes the entire population
 *		Randomly defines the initial position of each individual
 *		@author Guillaume GARBAY and Lauriane Massardier
 *		@version 1.0
 */
public class FishesPopulateProcess extends AquaNismsGroupProcess<Fish,FishesGroup> {
   
	//public int initialNumberOfFishes =5 ;	
	transient private UniformGen uniformGen;
	
	/**
	 *		Initialization procedure for the uniformGen variable
	 *		@param pilot
	 *		@return /
	 */
	@InitTransientParameters
	public void initTransientParameters (Pilot pilot) {
		double nbCell = (double) this.getGroup().getEnvironment().getCells().length;
		uniformGen = new UniformGen(pilot.getRandomStream(), new UniformDist(0,nbCell-1));
	}

	/**
	 *		Procedure that places each individual in the lake
	 *		Called during program initialization
	 *		@param group Represente all individuals
	 *		@return /
	 */
	public void doProcess (FishesGroup group) {
		
		int indexCell;
		Cell[] cells = group.getEnvironment().getCells();
		
		for (int i=0; i < group.getInitialNumberOfFishes() ;i++){
			
			// Declaration of initial age and weight for each individual
			int ageAtCreation = (int)(12+group.getPilot().getCurrentTime()-group.getMonthOfBirth())%12 +12*(i%5);
			double weightAtCreation = 2*group.getWeightAtAgeThreshold() * (ageAtCreation+12) / 12;
			
			// Place the individual in a cell with an HSI> 0, ie inside the lake
			do { indexCell = (int) Math.round(uniformGen.nextDouble());				
			} while (cells[indexCell].getHabitatQuality() < 0);
			
			// Creation of the new individual
			Fish newFish = new Fish(group.getPilot(), cells[indexCell], ageAtCreation, weightAtCreation);
    		group.addAquaNism(newFish);
		}		
	}
}
