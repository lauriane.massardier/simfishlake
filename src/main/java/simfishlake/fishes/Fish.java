/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake.fishes;

import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.simaqualife.pilot.Pilot;
import simfishlake.Cell;
import simfishlake.Individual;

public class Fish extends Individual {
    private double ingestedFood=0.;
	
	public static int cptIndividu = 1;
	public int idIndividu = 0;
	
    public Fish (Pilot pilot, Cell cell) {
    	//TODO fix weight according to the weightAtAgeThreshold
        // Default value of weight at birth
    	this(pilot, cell, 0, 10.);
    	idIndividu = cptIndividu ++;
    }
    
    public Fish (Pilot pilot, Cell cell, int age, double weight) {
        super(pilot, cell);
        this.age =age;
        this.weight = weight;
        idIndividu = cptIndividu ++;
    }
    
    @Observable(description="fish id")
    public final int getIdIndividu() {
        return idIndividu;
    }

    @Observable(description="cell index")
    public int getCellIndex() {
        return getPosition().getIndex();
    }
    
    @Observable(description="cell habitat quality")
    public double getCellHabitatQuality() {
        return getPosition().getHabitatQuality();
    }
	public double getIngestedFood() {
		return ingestedFood;
	}

	public void setIngestedFood (double ingestedFood) {
		this.ingestedFood = ingestedFood;
	}
	public void addIngestedFood (double ingestedFood) {
		this.ingestedFood += ingestedFood;
	}
	
	public double getSuitabilityForFish (Cell cell){
		if (cell.getFishes().size()>0){
                        //System.out.println(cell.getFishes(cell).size());
			return -1.; // at least an other fish in the cell
                }else
		{
			return cell.getHabitatQuality();
		}
			//return((double) cell.getPreys().size()) * cell.getHabitatQuality(); // number of preys accessible
	}
	
	
}