/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake.fishes;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

public class FishObservationProcess extends AquaNismsGroupProcess<Fish,FishesGroup> {
 public void doProcess (FishesGroup fishesGroup) {
		System.out.println("FishObservationProcess");
		
		fishesGroup.calculateFishesBiomass();
		fishesGroup.calculateFishesNumber();
		
	}
}
