/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake.fishes;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

public class FishGrowthProcess extends AquaNismsGroupProcess<Fish,FishesGroup> {
/**
	 * <code>convertionFactor</code> proportion of the ingested food transformed into pike weight 
	 */
	private double convertionFactor = 0.25;
	private double slimRate = 0.90;
	
	/* (non-Javadoc)
	 * @see fr.cemagref.simaqualife.kernel.processes.Process#doProcess(java.lang.Object)
	 */
	@Override
	public void doProcess (FishesGroup group) {
		System.out.println("FishGrowthProcess");
		
		for (Fish fish : group.getAquaNismsList()){
			fish.incAge();
			//System.out.print("  "+ (double) pike.getAge()/12. +"y "+pike.getWeight()+ " " );
			fish.setWeight( fish.getWeight() * slimRate + fish.getIngestedFood()* convertionFactor);
			//System.out.print(pike.getIngestedFood() +" " + pike.getWeight());
			//double ratio = 12* pike.getWeight()/(pike.getAge()+12);
			//System.out.println("("+ ((double) Math.round(ratio*100))/100 +")");
			fish.setIngestedFood(0.0);
		}
	}

}