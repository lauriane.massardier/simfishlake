/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake.fishes;

import fr.cemagref.simaqualife.kernel.processes.LoopAquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;

import java.util.ArrayList;
import java.util.List;
import simfishlake.Cell;
import umontreal.iro.lecuyer.probdist.UniformDist;
import umontreal.iro.lecuyer.randvar.UniformGen;
import fr.cemagref.simaqualife.pilot.Pilot;
import java.util.Comparator;

public class FishMovement extends LoopAquaNismsGroupProcess<Fish, FishesGroup> {
  //@SuppressWarnings("unused")
	
    transient private UniformGen uniformGen;

    public FishMovement (Pilot pilot) {
        uniformGen = new UniformGen(pilot.getRandomStream(), new UniformDist());
    }

    @InitTransientParameters
    public void initTransientParameters (Pilot pilot) {
        uniformGen = new UniformGen(pilot.getRandomStream(), new UniformDist(0, 1));
    }
    
  
    
    @Override     
    protected void doProcess (Fish fish, FishesGroup group) {    
        // Recovering the hsi value of the cell occupied by the fish
        double cellSuitability = fish.getSuitabilityForFish(fish.getPosition());
       
        // Calculation of the list of accessible cells 
        final List<Cell> surrounding = group.getEnvironment().getNeighbours(fish.getPosition());  
      
        // the first possiblity is the cell where the prey is
        List<Cell> possibilities = new ArrayList<Cell>();
        possibilities.add(fish.getPosition());
        
        // identify the destination possibilities in the neighbouring
        for (Cell cell : surrounding) {
            double currentCellSuitability = fish.getSuitabilityForFish(cell);
                            
            if (currentCellSuitability > 0 ) {
            	cellSuitability = currentCellSuitability;
                possibilities.add(cell);     
            }
        }
        
        switch (group.getMovementChoice()){ 
            
            // Movement type 0 : Random walk
            case 0:
                int randomPossibilitiesNumber = possibilities.size();           // size of available cells are all possible cells
                if (randomPossibilitiesNumber == 1) {                           //if there is only one cell then the individual moves in this cell
                    fish.moveTo(group.getPilot(), possibilities.get(0), group);
                } else {                                                        //otherwise random draw in the possible cells
                    int idx = (int)(uniformGen.nextDouble()* (double) randomPossibilitiesNumber);
                    fish.moveTo(group.getPilot(), possibilities.get(idx), group);
                }
                break;
                
                
            // Movement type 1 : ROfNBetterHSI each individual moves in a random cell belong to the n1 better HSI cells included in its Activity radius;	    
            case 1:
                possibilities.sort(Comparator.comparing(Cell::getHabitatQuality)); // sorts of all available cells     
                int number0fBestCell= (int) (group.getPercentOfCells()*possibilities.size())/100;//calculating the number of cells to recover according to the size of the sorted available cell list
                if(number0fBestCell==0){                                        //at least one cell = best hsi among possibilities 
                    number0fBestCell=1;    
                }
        
                int index=possibilities.size()-number0fBestCell;
                List<Cell> reducedBestPossibilities = new ArrayList<Cell>(possibilities.subList(index, possibilities.size()));
      
                // choose the destination cell
                int bestPossibilitiesNumber = reducedBestPossibilities.size();
                if (bestPossibilitiesNumber == 1) {
                    fish.moveTo(group.getPilot(), reducedBestPossibilities.get(0), group);
                } else {
                    int idx = (int)(uniformGen.nextDouble()* (double) bestPossibilitiesNumber);
                    fish.moveTo(group.getPilot(), reducedBestPossibilities.get(idx), group);
                }  
                break;
                
                
            // Movement type : 2 => BestHSIAmongNR  each individual moves in the best HSI value belong to n2 random cells of the those included in Activity Radius;                    
            case 2: 
                int numberOfHazardCell= (int) (group.getPercentOfCells()*possibilities.size())/100;//calcul du nombre de cellule a recuperer en fonction de la taille de la liste de cellule disponible triee
                if(numberOfHazardCell==0){
                    numberOfHazardCell=1;      
                }
       
                List<Cell> reducedPossibilities = new ArrayList<Cell>();
                    
                int i=0;
                int possibilitiesNumber = possibilities.size();
                if (possibilitiesNumber == 1) {
                    fish.moveTo(group.getPilot(), possibilities.get(0), group);
                } else {        
                    while(i <= numberOfHazardCell-1){
                        int indexCell=(int)(uniformGen.nextDouble()* (double) possibilitiesNumber);
                        if (!reducedPossibilities.contains(possibilities.get(indexCell))){
                            Cell id = possibilities.get(indexCell);
                            reducedPossibilities.add(id);
                            i++;
                        }  
                    }
                    reducedPossibilities.sort(Comparator.comparing(Cell::getHabitatQuality));
                    List<Cell> lastcell = new ArrayList<Cell>(reducedPossibilities.subList(reducedPossibilities.size()-1, reducedPossibilities.size()));                   
                    fish.moveTo(group.getPilot(), lastcell.get(0), group);
                }
                break;
            }
    }   
}