/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake.fishes;

import simfishlake.environment.Time;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;


/**
 *		Class that records the position of individuals at each time step
 *		The positions and the date corresponding to each are recorded in tables
 *		@author Guillaume GARBAY and Lauriane MASSARDIER
 *		@version 1.0
 */

public class FishTrackLocation extends AquaNismsGroupProcess<Fish, FishesGroup> {
    
       
    
	// Arrays of integer containing positions of each individuals and the current date
        public static int trackLocation[][] = new int [101][2159];
	public static int trackDate[][] = new int[5][2159];
	public static int cpt = 0;
	
	/**
	 *		Process that, at each time step, records the current date, the number of steps in the trackDate array
	 *		The positions of each individual are recorded in the trackLocation table
	 *		@param group represents all individuals
	 *		@return /
	 */		
	public void doProcess (FishesGroup group) {
		// Recording the date and the number of time step
		trackDate[0][cpt] = (int) group.getPilot().getCurrentTime();	// simulation time step
		trackDate[1][cpt] = Time.year;
		trackDate[2][cpt] = Time.month;
		trackDate[3][cpt] = Time.dayOfMonth;
		trackDate[4][cpt] = Time.hour;
		
		// Recording the position of individual
		for (Fish fish : group.getAquaNismsList())
			trackLocation[fish.idIndividu][cpt] = fish.getPosition().getIndex();
		
		cpt++;
	}

}
