/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simfishlake.fishes;

import java.util.ArrayList;
import java.util.List;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;


public class FishMortalityProcess extends AquaNismsGroupProcess<Fish, FishesGroup> {
    public void doProcess (FishesGroup group) {
		System.out.println("FishMortalityProcess");
		
		List<Fish> deadPike = new ArrayList<Fish>();
		for (Fish fish: group.getAquaNismsList()){
			double ratio = (12. * fish.getWeight() / (double) (fish.getAge()+12));

			if (ratio < group.getWeightAtAgeThreshold())
				deadPike.add(fish);
		}
	    // update the pikesGroup
		for (Fish fish: deadPike){
			group.getAquaNismsList().remove(fish);
			//ASK WHY
			fish.getPosition().removeFish(fish);
		}
	}
}
